def add(x,y):
    return x+y

def add(x,y,z):
    return x+y+z

print(add(10,20,30))
# print(add(10,30))               # first method won't work bcz of method overriding
# python doesn't support method overloading