class parent:
    name='Python'
    def hello(self):
        print('Function Inside Parent Class')

class child(parent):
    name='James Bond'
    def hello(self):
        print('Child Class Function')

ob=child()
ob.hello()
print(ob.name)