class personal_details:
    type='student'
    def __init__(self,name,city,roll_no):
        self.name=name
        self.city=city
        self.roll_no=roll_no

    def print_info(self):
        print('Name:{}, City:{}, Roll No:{}'.format(self.name,self.city,self.roll_no))

class library_details:
    def books(self,name,issue_date,return_date):
        print('Book Name: {} \nIssue Date: {} \nReturn Date: {}'.format(name,issue_date,return_date))

class fee_details(personal_details,library_details):
    print('Child Class Called')
    def detail(self,pending,submitted):
        print('Pending Fee: Rs{}/- \nSubmitted Fee: Rs{}/-'.format(pending,submitted) )

# st1=personal_details('Navi','Phagwara',10)
# st1.print_info()
# print(st1.type)

# st2=fee_details('Peter','abc',23)       # its parent class has constructor; so have to pass args to it
# st2.print_info()
# st2.detail(1000,2000)

student=fee_details('Peter Parker','abcd',1)
student.print_info()
student.books('Python Programming','15-03-2019','30-03-2019')
student.detail(0,20000)